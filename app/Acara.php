<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Acara extends Model{
    protected $table = 'acara';
    protected $primaryKey = 'id_acara';
        protected $fillable = [
        'nama_acara','pict','status','id_masjid'
    ];
}

?>