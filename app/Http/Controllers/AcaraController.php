<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Masjid;
use App\Acara;
use Auth;
class AcaraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
            $data = Acara::paginate('10');
            
            foreach ($data as $item) {
                $item->masjid = Masjid::find($item->id_masjid);
            }
            
        $tampil['data'] = $data;
            return view('acara.index',$tampil);
            // dd($tampil); 
        } else {
            $id = Auth::id();
            $cekmasjid = Masjid::where('id_user', $id)->first();
            $data = Acara::where('id_masjid', $cekmasjid->id_masjid)->paginate('10');
            foreach ($data as $item) {
                $item->masjid = Masjid::find($item->id_masjid);
            }
            $tampil['data'] = $data;
            return view('acara.index',$tampil);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
            $data['masjid'] = Masjid::get();
       
            return view('acara.create',$data);
            // dd($tampil); 
        } else {
            $id = Auth::id();
            $cekmasjid = Masjid::where('id_user', $id)->first();
            $data['masjid'] = Masjid::where('id_masjid', $cekmasjid->id_masjid)->get();
            
            return view('acara.create',$data);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'nama_acara' => 'required',
            'status' => 'required',
            'pict' => 'required',
            'id_masjid' => 'required',
        ]);

        $nama = Auth::user()->name;
        $file = $request->file('pict');
        if ($file !='') {
            $file = $request->file('pict');
            $extension=$file->getClientOriginalExtension();
            $destinationPath = 'assets/picture';
            $fileName = rand(11111, 99999) . '.' . $extension;
            $request->file('pict')->move($destinationPath, $fileName);
            $data = Acara::insert([
                'pict' => $fileName,
                'nama_acara' => $request->nama_acara,
                'id_masjid' => $request->id_masjid,
                'status' => $request->status
            ]);
        }else{
            $data = Acara::insert([
                'nama_acara' => $request->nama_acara,
                'id_masjid' => $request->id_masjid,
                'status' => $request->status
            ]);
        }
        return redirect()->route("acara.index")->with(
            "success",
            "Data berhasil disimpan."
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
            $data = Acara::findOrfail($id);
            $data->masjid = Masjid::get();
            return view('acara.edit',$data);
            // dd($data); 
        } else {
            $id_user = Auth::id();
            $cekmasjid = Masjid::where('id_user', $id_user)->first();
            $data = Acara::findOrfail($id);
            $data->masjid = Masjid::where('id_masjid', $cekmasjid->id_masjid)->get();
            
            return view('acara.edit',$data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_acara' => 'required',
            'status' => 'required',
            'id_masjid' => 'required',
        ]);

        $nama = Auth::user()->name;
        $file = $request->file('pict');
        if ($file !='') {
            $file = $request->file('pict');
            $extension=$file->getClientOriginalExtension();
            $destinationPath = 'assets/picture';
            $fileName = rand(11111, 99999) . '.' . $extension;
            $request->file('pict')->move($destinationPath, $fileName);
            $data = Acara::where('id_acara',$id)
            ->update([
                'pict' => $fileName,
                'nama_acara' => $request->nama_acara,
                'id_masjid' => $request->id_masjid,
                'status' => $request->status
            ]);
        }else{
            $data = Acara::where('id_acara',$id)
            ->update([
                'nama_acara' => $request->nama_acara,
                'id_masjid' => $request->id_masjid,
                'status' => $request->status
            ]);
        }
        return redirect()->route("acara.index")->with(
            "success",
            "Data berhasil disimpan."
            );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataAcara = Acara::findOrFail($id);
        $dataAcara->delete();
    }
}
