<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Kas;
use App\Masjid;


class KasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
            $data = Kas::paginate('10');
            
            foreach ($data as $item) {
                $item->masjid = Masjid::find($item->id_masjid);
            }
            
        $tampil['data'] = $data;
            return view('kas.index',$tampil);
            // dd($tampil); 
        } else {
            $id = Auth::id();
            $cekmasjid = Masjid::where('id_user', $id)->first();
            $data = Kas::where('id_masjid', $cekmasjid->id_masjid)->paginate('10');
            foreach ($data as $item) {
                $item->masjid = Masjid::find($item->id_masjid);
            }
            $tampil['data'] = $data;
            return view('kas.index',$tampil);
            // dd($data);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
            $data['masjid'] = Masjid::get();
       
            return view('kas.create',$data);
            // dd($tampil); 
        } else {
             $id = Auth::id();
            $cekmasjid = Masjid::where('id_user', $id)->first();
            $data['masjid'] = Kas::where('id_masjid', $cekmasjid->id_masjid)->get();
            
            return view('kas.create',$data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //validasi inputan
         $this->validate($request, [
            'tanggal' => 'required',
            'id_masjid' => 'required',
            'catatan' => 'required'
            ]);

            $dataKas = Kas::create($request->all());
            return redirect()->route("kas.index")->with(
            "success",
            "Data berhasil disimpan."
            );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
            $data = Kas::findOrfail($id);
            $data->masjid = Masjid::get();
            return view('kas.edit',$data);
            // dd($tampil); 
        } else {
            $id_user = Auth::id();
            $cekmasjid = Masjid::where('id_user', $id_user)->first();
            $data = Kas::findOrfail($id);
            $data->masjid = Masjid::where('id_masjid', $cekmasjid->id_masjid)->get();
            
            return view('kas.edit',$data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validasi inputan
        $this->validate($request, [
            'tanggal' => 'required',
            'id_masjid' => 'required',
            'catatan' => 'required'
            ]);

            $dataKas = Kas::findOrFail($id);
            $dataKas->id_masjid = $request->id_masjid;
            $dataKas->tanggal = $request->tanggal;
            $dataKas->uang_keluar = $request->uang_keluar;
            $dataKas->catatan = $request->catatan;
            $dataKas->save();

         
        return redirect()->route("kas.index")->with(
        "success",
        "Data berhasil diubah."
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $datakas = Kas::findOrFail($id);
        $datakas->delete();
    }
}
