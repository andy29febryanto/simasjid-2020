<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Masjid;
use App\Kas;
use DB;
class LaporanController extends Controller
{
    function index(Request $request)
    {
     
     if(request()->ajax())
     {
      if(!empty($request->from_date))
      {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
          $data = DB::table('kas_masjid')
          ->leftjoin('masjid','masjid.id_masjid','=','kas_masjid.id_masjid')
          ->whereBetween('tanggal', array($request->from_date, $request->to_date))
          ->orderBy('masjid.id_masjid', 'DESC')
          ->get();
        }else{
          $id = Auth::id();
          $cekmasjid = Masjid::where('id_user', $id)->first();
          $data = DB::table('kas_masjid')
          ->leftjoin('masjid','masjid.id_masjid','=','kas_masjid.id_masjid')
          ->where('masjid.id_masjid',$cekmasjid->id_masjid)
          ->whereBetween('tanggal', array($request->from_date, $request->to_date))
          ->get();
        }
       
      }else
      {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
          $data = DB::table('kas_masjid')
          ->leftjoin('masjid','masjid.id_masjid','=','kas_masjid.id_masjid')
          ->orderBy('masjid.id_masjid', 'DESC')
          ->get();
        }else{
          $id = Auth::id();
          $cekmasjid = Masjid::where('id_user', $id)->first();
          $data = DB::table('kas_masjid')
          ->leftjoin('masjid','masjid.id_masjid','=','kas_masjid.id_masjid')
          ->where('masjid.id_masjid',$cekmasjid->id_masjid)
          ->get();
        }
        
      }
      return datatables()->of($data)->make(true);
     }
     return view('laporan.index');
    }
   
}
