<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Masjid;
use App\User;
use Auth;

class MasjidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
            $data = Masjid::paginate('10');
            
            foreach ($data as $item ){
                $item->user = User::find($item->id_user);
            }
            $tampil['data'] = $data;
            return view('masjid.index', $tampil);
        
        } else {
            $id = Auth::id();
            $cekmasjid = Masjid::where('id_user', $id)->first();
            $data = Masjid::where('id_masjid', $cekmasjid->id_masjid)->paginate('10');
            foreach ($data as $item ){
                $item->user = User::find($item->id_user);
            }
            $tampil['data'] = $data;
            return view('masjid.index',$tampil);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['user'] = User::where('hak_akses', 'petugas')->get();
        return view('masjid.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi inputan
        $this->validate($request, [
            'nama_masjid' => 'required',
            'alamat' => 'required',
            'no_telpon' => 'required',
            'no_rekening' => 'required',
            'id_user' => 'required|unique:masjid',
            ]);

            $dataMasjid = Masjid::create($request->all());
            return redirect()->route("masjid.index")->with(
            "success",
            "Data berhasil disimpan."
            ); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cekauth = Auth::user()->hak_akses;
        if ($cekauth == 'administrator') {
           
        $data = Masjid::findOrFail($id);
        $data->user = User::where('hak_akses', 'petugas')->get();

        //tampilkan resources/views/Jamaat/edit.blade.php
        return view("masjid.edit", $data);

        } else {
            $id_user = Auth::id();
            $cekmasjid = Masjid::where('id_user', $id_user)->first();
            $data = Masjid::findOrFail($id);
            $data->user = User::where('hak_akses', 'petugas')
            ->where('id',$cekmasjid->id_user)
            ->get();

        //tampilkan resources/views/Jamaat/edit.blade.php
        return view("masjid.edit", $data);

        }




    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //validasi inputan
         $this->validate($request, [
            'nama_masjid' => 'required',
            'alamat' => 'required',
            'no_telpon' => 'required',
            'no_rekening' => 'required',
            'id_user' => 'required|unique:masjid',
            ]);

            $dataMasjid = Masjid::findOrFail($id);
            $dataMasjid->nama_masjid = $request->nama_masjid;
            $dataMasjid->alamat = $request->alamat;
            $dataMasjid->no_rekening = $request->no_rekening;
            $dataMasjid->no_telpon = $request->no_telpon;
            $dataMasjid->id_user = $request->id_user;
            $dataMasjid->save();

         
        return redirect()->route("masjid.index")->with(
        "success",
        "Data berhasil diubah."
        ); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataMasjid = Masjid::findOrFail($id);
            $dataMasjid->delete();
    }
}
