<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Karangan extends Model{
    protected $table = 'karangan';
    protected $primaryKey = 'id_karangan';
        protected $fillable = [
        'id_user','judul','isi','pict','status'
    ];
}

?>