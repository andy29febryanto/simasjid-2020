<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Kas extends Model{
    protected $table = 'kas_masjid';
    protected $primaryKey = 'id_kas';
        protected $fillable = [
        'uang_masuk','uang_keluar','tanggal','catatan','id_masjid'
    ];

    
    protected $dates = ['tanggal'];
}

?>