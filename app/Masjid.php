<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Masjid extends Model{
    protected $table = 'masjid';
    protected $primaryKey = 'id_masjid';
        protected $fillable = [
        'nama_masjid','alamat','no_rekening','no_telpon','id_user'
    ];
}

?>