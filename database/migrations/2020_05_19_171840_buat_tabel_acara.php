<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatTabelAcara extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acara', function (Blueprint $table) {
            $table->increments('id_acara');
            $table->integer('id_masjid')->unsigned();
            $table->string('nama_acara');
            $table->string('pict');
            $table->enum('status', ['publish', 'hide']);
            $table->timestamps();


            $table->foreign('id_masjid')->references('id_masjid')->on('masjid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acara');
    }
}
