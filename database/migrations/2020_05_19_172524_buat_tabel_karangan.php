<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatTabelKarangan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karangan', function (Blueprint $table) {
            $table->increments('id_karangan');
            $table->bigInteger('id_user')->unsigned();
            $table->string('judul');
            $table->string('isi');
            $table->string('pict');
            $table->enum('status', ['publish', 'hide']);
            $table->timestamps();


            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karangan');
    }
}
