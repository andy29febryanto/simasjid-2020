<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatTabelKasMasjid extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kas_masjid', function (Blueprint $table) {
            $table->increments('id_kas');
            $table->integer('id_masjid')->unsigned();
            $table->date('tanggal');
            $table->bigInteger('uang_masuk');
            $table->bigInteger('uang_keluar');
            $table->string('catatan');
            $table->timestamps();

            $table->foreign('id_masjid')->references('id_masjid')->on('masjid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kas_masjid');
    }
}
