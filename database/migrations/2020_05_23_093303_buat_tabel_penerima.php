<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BuatTabelPenerima extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penerima', function (Blueprint $table) {
            $table->increments('id_penerima');
            $table->integer('id_masjid')->unsigned();
            $table->string('nama_penerima');
            $table->string('alamat');
            $table->enum('jenis_kelamin', ['L', 'P']);
            $table->bigInteger('usia')->unsigned();
            $table->string('keterangan');
            $table->timestamps();

            $table->foreign('id_masjid')->references('id_masjid')->on('masjid');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penerima');
    }
}
