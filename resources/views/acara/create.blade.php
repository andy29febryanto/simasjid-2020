@extends('adminlte::page')
@section('title','acara')
@section('content_header')
    <h1 class="m-0 text-dark">Manajemen acara</h1>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            @if ($errors->any())
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" data-dismiss="alert" class="close" aria-hidden="true">x</button>
                    <h4><i class="icon fa fa-warning"></i>Perhatian!</h4>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            
            <div class="card">
                <div class="card-header">
                    Tambah acara
                </div>
                <div class="card-body">
                    <form action="{{ route('acara.store') }}" enctype="multipart/form-data" method="post" class="form-horizontal">
                        @include('acara.form')
                    </form>
                </div>
            </div>

        </div> 
    </div>
    @stop
    @section('plugins.Pace', true)
   