@extends('adminlte::page')
@section('title','Acara')
@section('content_header')
    <h1 class="m-0 text-dark">Manajemen Acara</h1>
    @endsection
@section('content')

<div class="row">
        <div class="col-12">
            @if($errors->any())
            <div class="alert alert-warning alert-dismissable">
                <button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-warning"></i>Perhatian!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    Ubah Acara
                </div>
                <div class="card-body">
                    <form action = "{{ route('acara.update', $id_acara ?? '') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @csrf
                          <div class="form-group">
                            <label for="id_masjid" class="col-sm-2 cotrol-label">masjid</label>
                            <div class="col-sm-10">
                                <select name="id_masjid" class="form-control">
                                    @foreach ($masjid as $item)
                                        <option value="{{ $item->id_masjid }}" {{ ( ($id_masjid ?? ''   ) == $item->id_masjid ) ? 'selected' : '' }}>
                                            {{ $item->id_masjid }} - {{ $item->nama_masjid }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nama_acara" class="col-sm-2 cotrol-label">Acara</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nama_acara" value="{{ $nama_acara ?? ''  }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-xl-3 col-lg-3 col-form-label">Foto </label>
                            <div class="col-lg-9 col-xl-6">
                                <img class="kt-avatar__holder" src="/assets/picture/{{$pict}}" id="showgambar" style="height:300px;" alt="">
                                <input type="file" id="inputgambar" name="pict" value="{{$pict}}" class="validate"/ >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Status">Status</label>
                            <select class="form-control" name="status">
                                <option value="publish" @if ($status == 'publish' )
                                    selected>
                                    Publish
                                </option>
                                <option value="hide">Hide</option>
                                @else
                                <option value="hide" selected>Hide</option>
                                <option value="publish">Publish</option>
                                
                                @endif
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
                                <a href="{{ route('acara.index') }}" role="button" class="btn btn-primary">Batal</a>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>

        </div>
    </div>

 @endsection
 @section('plugins.Pace', true)
 @section('js')
 <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

 <script type="text/javascript">

    function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#showgambar').attr('src', e.target.result);
          }

          reader.readAsDataURL(input.files[0]);
      }
  }

  $("#inputgambar").change(function () {
      readURL(this);
  });


</script>
