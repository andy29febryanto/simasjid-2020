{{ csrf_field() }}
<div class="form-group">
    <label for="id_masjid" class="col-sm-2 cotrol-label">Masjid</label>
    <div class="col-sm-10">
        <select name="id_masjid" class="form-control">
            @foreach ($masjid as $item)
                <option value="{{ $item->id_masjid }}" {{ ( ($id_masjid ?? ''   ) == $item->id_masjid ) ? 'selected' : '' }}>
                    {{ $item->id_masjid }} - {{ $item->nama_masjid }}
                </option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="nama_acara" class="col-sm-2 cotrol-label">Acara</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="nama_acara" value="{{ $nama_acara ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="pict" class="col-sm-2 cotrol-label">Banner</label>
    <div class="col-sm-10">
        <input type="file" class="form-control" name="pict" value="{{ $pict ?? ''  }}">
    </div>
</div>

<div class="form-group">
    <label for="status" class="col-sm-2 cotrol-label">Status</label>
    <div class="col-sm-10">
        <select name="status" class="form-control">
            <option value="hide"> Hide </option>
            <option value="publish"> Publish </option>
        </select>
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('acara.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>
