@extends('adminlte::page')
@section('title','Kas')
@section('content_header')
    <h1 class="m-0 text-dark">Manajemen Kas</h1>
    @endsection
@section('content')

<div class="row">
        <div class="col-12">
            @if($errors->any())
            <div class="alert alert-warning alert-dismissable">
                <button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-warning"></i>Perhatian!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    Ubah Kas
                </div>
                <div class="card-body">
                    <form action = "{{ route('kas.update', $id_kas ?? '') }}" class="form-horizontal" method="post">
                        @method('PUT')
                        @csrf
                          <div class="form-group">
                            <label for="id_masjid" class="col-sm-2 cotrol-label">masjid</label>
                            <div class="col-sm-10">
                                <select name="id_masjid" class="form-control">
                                    @foreach ($masjid as $item)
                                        <option value="{{ $item->id_masjid }}" {{ ( ($id_masjid ?? ''   ) == $item->id_masjid ) ? 'selected' : '' }}>
                                            {{ $item->id_masjid }} - {{ $item->nama_masjid }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="tanggal" class="col-sm-2 cotrol-label">Tanggal</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="tanggal" name="tanggal" value="{{ \Carbon\Carbon::parse($item->tanggal)->format('m/d/Y') ?? '' }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="uang_masuk" class="col-sm-2 cotrol-label">Uang Masuk</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="uang_masuk" value="{{ $uang_masuk ?? ''  }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="uang_keluar" class="col-sm-2 cotrol-label">Uang keluar</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="uang_keluar" value="{{ $uang_keluar ?? ''  }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="catatan" class="col-sm-2 control-label">Catatan</label>
                            <div class="col-sm-10">
                                <input type="text" name="catatan" class="form-control" value="{{ $catatan ?? '' }}">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
                                <a href="{{ route('kas.index') }}" role="button" class="btn btn-primary">Batal</a>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>

        </div>
    </div>
 @endsection
 @section('plugins.Pace', true)
 @section('plugins.DateRangePicker', true)
 @section('js')

 <script>
    $(function() {
      $('input[name="tanggal"]').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 1901,
        maxYear: parseInt(moment().format('YYYY'),10)
      }); 
    });
    </script>
 @stop