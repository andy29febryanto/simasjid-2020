{{ csrf_field() }}
<div class="form-group">
    <label for="id_masjid" class="col-sm-2 cotrol-label">Masjid</label>
    <div class="col-sm-10">
        <select name="id_masjid" class="form-control">
            @foreach ($masjid as $item)
                <option value="{{ $item->id_masjid }}" {{ ( ($id_masjid ?? ''   ) == $item->id_masjid ) ? 'selected' : '' }}>
                    {{ $item->id_masjid }} - {{ $item->nama_masjid }}
                </option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="tanggal" class="col-sm-2 cotrol-label">Tanggal</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="tanggal" name="tanggal" value="{{ $tanggal ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="uang_masuk" class="col-sm-2 cotrol-label">Uang Masuk</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="uang_masuk" value="{{ $uang_masuk ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="uang_keluar" class="col-sm-2 cotrol-label">Uang Keluar</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="uang_keluar" value="{{ $uang_keluar ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="catatan" class="col-sm-2 control-label">Catatan</label>
    <div class="col-sm-10">
        <input type="text" name="catatan" class="form-control" value="{{ $catatan ?? '' }}">
    </div>
</div>

<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('kas.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>
