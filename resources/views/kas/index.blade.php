@extends('adminlte::page')

@section('title','kas')

@section('content_header')
<h1 class="m-0 text-dark">Manajemen kas</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    <a href="{{ route('kas.create') }}" class="btn btn-primary btn-md">
                        <i class="fa fa-plus"> Tambah</i>
                    </a>                        
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 20px">#</th>
                                <th> tanggal</th>
                                <th> id - Nama Masjid</th>
                                <th>uang masuk</th>
                                <th>uang keluar</th>
                                <th> catatan</th>
                                
                                <th style="width: 80px"> Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no= 1 ; ?>
                            @forelse ($data as $item)
                                <tr>
                                    <td>
                                        {{ $no }}
                                    </td>
                                    <td>
                                         {{ \Carbon\Carbon::parse($item->tanggal)->format('d/m/Y')}}
                                    </td>
                                    <td>
                                        {{ $item->masjid->id_masjid }} - {{ $item->masjid->nama_masjid }}
                                    </td>
                                    
                                    <td>
                                        {{ $item->uang_masuk }}
                                    </td>
                                    <td>
                                        {{ $item->uang_keluar }}
                                    </td>
                                    <td>
                                        {{ $item->catatan }}
                                    </td>
                                    
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('kas.edit', $item->id_kas) }}" class="btn btn-success">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            <a onclick="hapus('{{ $item->id_kas }}')" href="#" class="btn btn-primary">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @empty
                                <tr>
                                    <td colspan="7">
                                        Tidak Ada Data
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix text-right">
                    {{ $data->links() }}
                </div>

            </div>
        </div>
    </div>
    @stop

    @section('plugins.Sweetalert2', true)
    @section('plugins.Pace',true)
    
    @section('js')
        @if (session('success'))
            <script type="text/javascript">
                Swal.fire(
                    'Sukses!',
                    '{{ session('success') }}',
                    'success'
                )
            </script>
        @endif
            <script type="text/javascript">
                function hapus(id){
                    Swal.fire({
                        title : 'Konfirmasi',
                        text : "Yakin ingin menghapus data ini ?",
                        icon : 'warning',
                        showCancelButton : true,
                        confirmButtonColor : '#3085d6',
                        cancelButtonColor: '#dd3333',
                        confirmButtonText: 'Hapus',
                        cancelButtonText: 'Batal',
                    }).then((result) =>  {
                        if (result.value){
                            $.ajax({
                                url: "/kas/"+id,
                                type: 'DELETE',
                                data:{
                                    '_token' : $('meta[name=csrf-token]').attr("content"),
                                },
                                success : function(result){
                                    Swal.fire(
                                        'Sukses!',
                                        'Berhasil dihapus',
                                        'success'
                                    );
                                    location.reload();
                                }
                            })
                        }
                    })
                }
            </script>
            @stop