@extends('adminlte::page')

@section('title','laporan')


@section('content')
<div class="row">
    <div class="col-12">

        <div class="card">
            <div class="card-header">
                    <div class="row input-daterange">
                        <div class="col-md-4">
                            <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" readonly />
                        </div>
                        <div class="col-md-4">
                            <input type="text" name="to_date" id="to_date" class="form-control" placeholder="To Date" readonly />
                        </div>
                        <div class="col-md-4">
                            <button type="button" name="filter" id="filter" class="btn btn-primary">Filter</button>
                            <button type="button" name="refresh" id="refresh" class="btn btn-default">Refresh</button>
                        </div>    
                    </div>                                      
            </div>
            <div class="card-body">
                <table class="display table table-bordered table-striped" style="width:100%" id="kas_table">
                    <thead>
                        <tr>
                            <th> tanggal</th>
                            <th> masjid</th>
                            <th>uang masuk</th>
                            <th>uang keluar</th>
                            <th> catatan</th>
                         </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th colspan="2" style="text-align:left">Total:</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
       
        </div>
    </div>
</div>
 @stop
 
 @section('plugins.Pace',true)
 @section('plugins.Datatables',true)
 
@section('js')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script>
    $(document).ready(function(){
     $('.input-daterange').datepicker({
      todayBtn:'linked',
      format:'yyyy-mm-dd',
      autoclose:true
     });
    
     load_data();
    
     function load_data(from_date = '', to_date = '')
     {
      $('#kas_table').DataTable({
       
        processing: true,
       serverSide: true,
       ajax: {
        url:'{{ route("laporan.index") }}',
        data:{from_date:from_date, to_date:to_date}
       },
       columns: [
        {
         data:'tanggal',
         name:'tanggal'
        },
        {
            data:'nama_masjid',
            name:'nama_masjid'
           },
        {
         data:'uang_masuk',
         name:'uang_masuk'
        },
        {
            data:'uang_keluar',
            name:'uang_keluar'
           },
           {
            data:'catatan',
            name:'catatan'
           }
       ],
       paging: false,
        dom: 'Bfrtip',
        "footerCallback": function (tfoot, data, start, end, display) {
            var api = this.api();
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
            var p = api.column(2).data()
            .reduce(function (a, b) {
                return intVal(a) + intVal(b);
            },0 )
            $(api.column(2).footer()).html("Total uang masuk: "+ p);
            var q = api.column(3).data().reduce(function (a, b) {
                return intVal(a) + intVal(b);
            },0 )
            $(api.column(3).footer()).html("Total uang keluar: "+ q);
           
           
            var r = p + q;
            
            $(api.column(4).footer()).html("Sisa Saldo: "+ r);

        },

        buttons: [
            { extend: 'copyHtml5', footer: true },
            { extend: 'excelHtml5', footer: true },
            { extend: 'csvHtml5', footer: true },
            { extend: 'pdfHtml5', footer: true }
        ]
      
      });
     }
    


     $('#filter').click(function(){
      var from_date = $('#from_date').val();
      var to_date = $('#to_date').val();
      if(from_date != '' &&  to_date != '')
      {
       $('#kas_table').DataTable().destroy();
       load_data(from_date, to_date);
      }
      else
      {
       alert('Both Date is required');
      }
     });
    
     $('#refresh').click(function(){
      $('#from_date').val('');
      $('#to_date').val('');
      $('#kas_table').DataTable().destroy();
      load_data();
     });
    
    }); 
    </script>
    
@stop