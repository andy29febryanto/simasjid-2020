@extends('adminlte::page')
@section('title','Masjid')
@section('content_header')
    <h1 class="m-0 text-dark">Manajemen Masjid</h1>
    @endsection
@section('content')
    <div class="row">
        <div class="col-12">
            @if($errors->any())
            <div class="alert alert-warning alert-dismissable">
                <button class="close" type="button" data-dismiss="alert" aria-hidden="true">x</button>
                <h4><i class="icon fa fa-warning"></i>Perhatian!</h4>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    Ubah Masjid
                </div>
                <div class="card-body">
                    <form action = "{{ route('masjid.update', $id_masjid ?? '') }}" class="form-horizontal" method="post">
                        @method('PUT')
                        @csrf

                        <div class="form-group">
                            <label for="nama_masjid" class="col-sm-2 cotrol-label">Nama Masjid</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nama_masjid" value="{{ $nama_masjid ?? ''  }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alamat" class="col-sm-2 cotrol-label">Alamat</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="alamat" value="{{ $alamat ?? ''  }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="no_rekening" class="col-sm-2 cotrol-label">No Rekening</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="no_rekening" value="{{ $no_rekening ?? ''  }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="no_telpon" class="col-sm-2 control-label">No Telpon</label>
                            <div class="col-sm-10">
                                <input type="text" name="no_telpon" class="form-control" value="{{ $no_telpon ?? '' }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="id_user" class="col-sm-2 cotrol-label">Petugas</label>
                            <div class="col-sm-10">
                                <select name="id_user" class="form-control">
                                    @foreach ($user as $item)
                                        <option value="{{ $item->id }}" {{ ( ($id_user ?? ''   ) == $item->id ) ? 'selected' : '' }}>
                                            {{ $item->id }} - {{ $item->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="col-sm-10 col-sm-offset-2">
                                <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
                                <a href="{{ route('masjid.index') }}" role="button" class="btn btn-primary">Batal</a>
                            </div>
                        </div>
                    </form>
                    
                </div>
            </div>

        </div>
    </div>
 @endsection
 @section('plugins.Pace',true)