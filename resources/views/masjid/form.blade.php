{{ csrf_field() }}
<div class="form-group">
    <label for="nama_masjid" class="col-sm-2 cotrol-label">Nama Masjid</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="nama_masjid" value="{{ $nama_masjid ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="alamat" class="col-sm-2 cotrol-label">Alamat</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="alamat" value="{{ $alamat ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="no_rekening" class="col-sm-2 cotrol-label">No Rekening</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="no_rekening" value="{{ $no_rekening ?? ''  }}">
    </div>
</div>
<div class="form-group">
    <label for="no_telpon" class="col-sm-2 control-label">No Telpon</label>
    <div class="col-sm-10">
        <input type="text" name="no_telpon" class="form-control" value="{{ $no_telpon ?? '' }}">
    </div>
</div>
<div class="form-group">
    <label for="id_user" class="col-sm-2 cotrol-label">Petugas</label>
    <div class="col-sm-10">
        <select name="id_user" class="form-control">
            @foreach ($user as $item)
                <option value="{{ $item->id }}" {{ ( ($id_user ?? ''   ) == $item->id ) ? 'selected' : '' }}>
                    {{ $item->id }} - {{ $item->name }}
                </option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10 col-sm-offset-2">
        <input type="submit" class="btn btn-md btn-success" name="simpan" value="Simpan">
        <a href="{{ route('masjid.index') }}" role="button" class="btn btn-primary">Batal</a>
    </div>
</div>