@extends('adminlte::page')

@section('title','masjid')

@section('content_header')
<h1 class="m-0 text-dark">Manajemen masjid</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">

            <div class="card">
                <div class="card-header">
                    @if (Auth::user() === "admin" )
                    <a href="{{ route('masjid.create') }}" class="btn btn-primary btn-md">
                        <i class="fa fa-plus"> Tambah</i>
                    </a>       
                    @else
                        
                    @endif
                                         
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th style="width: 20px">#</th>
                                <th> nama masjid</th>
                                <th> alamat</th>
                                <th> no.telpon - no.rekening</th>
                                <th> petugas</th>
                                <th style="width: 80px"> Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no= 1 ; ?>
                            @forelse ($data as $item)
                                <tr>
                                    <td>
                                        {{ $no }}
                                    </td>
                                    <td>
                                        {{ $item->nama_masjid }}
                                    </td>
                                    <td>
                                        {{ $item->alamat }}
                                    </td>
                                    <td>
                                        {{ $item->no_telpon }} - {{ $item->no_rekening }}
                                    </td>
                                    <td>
                                        {{ $item->user->id }} - {{ $item->user->name }}
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('masjid.edit', $item->id_masjid) }}" class="btn btn-success">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            @if (Auth::user() === "admin" )
                                            <a onclick="hapus('{{ $item->id_masjid }}')" href="#" class="btn btn-primary">
                                                <i class="fas fa-trash"></i>
                                            </a>
                                            @else
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                                <?php $no++; ?>
                            @empty
                                <tr>
                                    <td colspan="6">
                                        Tidak Ada Data
                                    </td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix text-right">
                    {{ $data->links() }}
                </div>

            </div>
        </div>
    </div>
    @stop

    @section('plugins.Sweetalert2', true)
    @section('plugins.Pace',true)
    
    @section('js')
        @if (session('success'))
            <script type="text/javascript">
                Swal.fire(
                    'Sukses!',
                    '{{ session('success') }}',
                    'success'
                )
            </script>
        @endif
            <script type="text/javascript">
                function hapus(id){
                    Swal.fire({
                        title : 'Konfirmasi',
                        text : "Yakin ingin menghapus data ini ?",
                        icon : 'warning',
                        showCancelButton : true,
                        confirmButtonColor : '#3085d6',
                        cancelButtonColor: '#dd3333',
                        confirmButtonText: 'Hapus',
                        cancelButtonText: 'Batal',
                    }).then((result) =>  {
                        if (result.value){
                            $.ajax({
                                url: "/masjid/"+id,
                                type: 'DELETE',
                                data:{
                                    '_token' : $('meta[name=csrf-token]').attr("content"),
                                },
                                success : function(result){
                                    Swal.fire(
                                        'Sukses!',
                                        'Berhasil dihapus',
                                        'success'
                                    );
                                    location.reload();
                                }
                            })
                        }
                    })
                }
            </script>
            @stop